# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html)[^1].

<!---
Types of changes

- Added for new features.
- Changed for changes in existing functionality.
- Deprecated for soon-to-be removed features.
- Removed for now removed features.
- Fixed for any bug fixes.
- Security in case of vulnerabilities.

-->

## [Unreleased]

## [1.2.3] - 2024-09-23

### Fixed

* The file format checking in relation factories, which prevented the activation of playblast rendering actions on new Blender files.

## [1.2.2] - 2024-09-10

### Fixed

* The *Auto-play playblast* option is temporarily hidden on Linux and MacOS.

## [1.2.1] - 2024-09-10

### Fixed

* The *Auto-play playblast* option is temporarily disabled on Linux and MacOS platforms as long as they are not supported in the LFS playblast Blender add-on.

## [1.2.0] - 2024-09-04

### Added

* A new action *Publish and Playblast* which uses the *ANPO Render Playblast* action. This action overrides the default action *Publish and Playblast* provided in Libreflow.

## [1.1.0] - 2024-08-29

### Added

* A toogleable option to autoplay a blender playblast after being rendered
* Playblast frames count now checks against what was defined on Kitsu

## [1.0.0] - 2024-05-16

### Added

* initial release
